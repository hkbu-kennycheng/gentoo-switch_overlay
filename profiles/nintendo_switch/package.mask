# Does not work with jetson-tx1-drivers-32.7
>x11-base/xorg-drivers-21
>x11-base/xorg-server-21

# Mesa
>media-libs/mesa-21.3.8

# 2023-10-14 Does not work with old xorg-server. Maybe can be solved by reverting
# https://cgit.freedesktop.org/xorg/driver/xf86-input-libinput/commit/?id=94a52a848801ff035ec4d1c604dac7c90708e562
>x11-drivers/xf86-input-libinput-1.4

#Mask newer libv4l/v4l-utils as patches will need ported forward
>media-libs/libv4l-1.18.1
>media-tv/v4l-utils-1.18.1

# Use version in overlay
>media-video/ffmpeg-4.4.9999

# Downgrade because of old linux headers from l4t kernel
>dev-libs/ell-0.30-r1
>net-libs/libpcap-1.9.1-r3
>sys-apps/systemd-utils-253
# 2023-08-18 - pidfd symbols not in kernel headers. https://bugs.gentoo.org/911375
>sys-libs/glibc-2.36
# 2023-08-18 - fails to build with glibc-2.35
>sys-devel/gcc-12

#Audio Stuff
>media-libs/alsa-lib-1.2.4
>media-libs/alsa-topology-conf-1.2.4
>media-libs/alsa-ucm-conf-1.2.4
>media-plugins/alsa-plugins-1.2.4
>media-sound/alsa-utils-1.2.4

#Weston / wayland
>dev-libs/weston-10.1.0
>x11-base/xwayland-23

# Since no overlay masks are supported in profile, the whole package is masked.
# The replaced versions in this overlay are unmasked by package.unmask
dev-qt/qtcore
net-wireless/bluez
sys-apps/shadow
sys-kernel/installkernel
x11-libs/libdrm

# 13.01.2024 - bell07 - does not build.
# https://gitlab.com/switchroot/bootstack/switch-uboot/-/issues/2
# Masked till the issue is solved. Use the sys-boot/switchroot-bootstack[ubuntu-bl33] instead
sys-boot/nintendo-switch-u-boot

# 13.01.2024 - bell07 - deprecation
# Not needed anymore to boot linux. Will be removed in some months
sys-boot/nintendo-switch-coreboot

# 13.01.2024 - bell07 - deprecation
# Abadoned. Please use sys-libs/switch-l4t-configs[dock-handler] instead
x11-misc/dock-hotplug
